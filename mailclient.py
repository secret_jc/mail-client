import socket
import base64
import email

def receive_long(sock, msg_leng):
    res = '';
    count = 0;
    while (count < msg_leng):
        msg = sock.recv(30000);
        count = count + len(msg);
        res = res + msg;
    return res;

def send_msg(sock, msg):
    sock.send(msg);
    s = sock.recv(1000);
    #print s;
    return s;

def authenticate(sock, username, password):
    send_msg(sock, 'AUTH LOGIN\r\n');
    msg = send_msg(sock, base64.b64encode(username) + '\r\n');
    if (msg[0:3] != '334'):
        return False;
    msg = send_msg(sock, base64.b64encode(password) + '\r\n');
    if (msg[0:3] != '235'):
        return False;
    return True;

def send_mail(sock, sender):
    count = 0;
    while (True):
        print "Type 0 if you want to return.";
        receiver = raw_input("Receiver's address: ");
        if (receiver == '0'):
            return count;
        send_msg(sock, 'mail from:<' + sender + '>\r\n');
        msg = send_msg(sock, 'RCPT to:<' + receiver + '>\r\n');  
        if (msg[0:3] != '250'):
            print "Invalid receiver! Please type again. ";
        else:
            sock.send('DATA\r\n');
            subject = raw_input("subject: ");
            sock.send('subject:' + subject + '\r\n');
            print "Type EXIT to quit typing.";
            text = '';
            while (True):
                te = raw_input('text: ');
                if (te == 'EXIT'):
                    break;
                else:
                    text = text + te + '\r\n';
            send_msg(sock, '\r\n' + text + '.\r\n');
            print "The Email has been sent. Now you can send another one."

def decode(string):
    msg = email.message_from_string(string);
    print "from: ", email.utils.parseaddr(msg.get("from"))[1];
    print "to: ", email.utils.parseaddr(msg.get("to"))[1];
    subject = msg.get('subject');
    subject = email.Header.decode_header(email.Header.Header(subject));
    print 'subject: ' + subject[0][0];
    count = 0;
    for par in msg.walk():
        if not par.is_multipart():
            print '------seperation------'
            File = par.get_param("name");
            if File:
                filename = email.Header.decode_header(email.Header.Header(File));
                print 'Attachment: ', filename[0][0];
                print 'Do you want to save the attachment? Type 0 for NO.';
                path = raw_input('path: ');
                if (path != '0'):
                    filename = email.Header.decode_header(email.Header.Header(File));
                    print 'Attachment: ', filename[0][0];
                    print 'Do you want to save the attachment? Type 0 for NO.';
                    count = 0;
                    while (True):
                        try:
                            path = raw_input('path: ');
                            if (path != '0'):
                                attachment = open(path, 'wb');
                                attachment.write(par.get_payload(decode=True));
                                attachment.close();
                            print "Saved successfully !"
                            break;
                        except:
                            print 'Invalid path !';
            else:
                if (count == 0):
                    print par.get_payload(decode=True);
                    count = count + 1;

def recv_mail(sock, username, password):
    send_msg(sock, 'stat\r\n');
    while (True):
        print "1. read mails./ 2. delete mails./ 3. reset deletion. / 0. quit."
        option = raw_input("option: ");
        if (option == '0'):
            return 1;
        if (option == '1'):
            No = raw_input("No. of mail: ");
            msg = send_msg(sock, 'list ' + No + '\r\n');
            msg_leng = int(msg[5 + len(No):]);
            sock.send('retr ' + No + '\r\n');
            sock.recv(1000);
            msg = receive_long(sock, msg_leng);
            decode(msg);
        if (option == '2'):
            No = raw_input("No. of mail: ");
            sock.send('dele ' + No + '\r\n');
            sock.recv(1000);
        if (option == '3'):
            send_msg(sock, 'rset\r\n');
            print "Reset successfully"
    return 1;

def email_client():
    port_smtp = 25;
    port_pop3 = 110;
    sock_smtp = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0);
    sock_pop3 = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0);
    while (True):
        try:
            host_addr = raw_input("Your host address: ");
            host_smtp = "smtp." + host_addr;
            host_pop3 = "pop3." + host_addr;
            sock_smtp.connect((host_smtp, port_smtp));
            sock_smtp.recv(1000);
            sock_pop3.connect((host_pop3, port_pop3));
            sock_pop3.recv(1000);
            print 'Connected successfully. ';
            break;
        except:
            print 'Connection failed !';
    send_msg(sock_smtp, 'helo ehlo\r\n');
    while (True):
        username = raw_input("username: ");
        password = raw_input("password: ");
        if (authenticate(sock_smtp, username, password)):
            print "Login successfully!";
            send_msg(sock_pop3, 'user ' + username + '\r\n');
            msg = send_msg(sock_pop3, 'pass ' + password + '\r\n');
            if (msg[0:3] == '+OK'):
                print "Your email has " + msg[4:];
                break;
            else:
                print msg;
        else:
            print "Wrong user name or password!";
            sock_smtp.close();
            sock_smtp = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0);
            sock_smtp.connect((host_smtp, port_smtp));
            sock_smtp.recv(1000);
            send_msg(sock_smtp, 'helo ehlo\r\n');
    while (True):
        print "1. send mails./ 2. mail box./ 0. quit."
        option = raw_input("Your option: ");
        if (option == '1'):
            send_mail(sock_smtp, username + '@' + host_addr);
        if (option == '2'):
            recv_mail(sock_pop3, username + '@' + host_addr, password);
        if (option == '0'):
            print "Thanks for using. Bye~"
            break;
        if (not (option in ['0', '1', '2'])):
            print "Wrong option. Please choose again."
    sock_smtp.close();
    send_msg(sock_pop3, 'QUIT\r\n');
    sock_pop3.close();

email_client();
